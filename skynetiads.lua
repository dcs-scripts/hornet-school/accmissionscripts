redIADS = SkynetIADS:create("RED")

redIADS:addSAMSitesByPrefix('IADS-SAM-Site')

redIADS:addEarlyWarningRadarsByPrefix('IADS-EWR')

redIADS:activate()

MESSAGE:New("SkyNet IADS is live. Version : 1.0. Mission Version : v2.4", 10, "INFO"):ToAll()