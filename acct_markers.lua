-- No MOOSE settings menu. Comment out this line if required.
_SETTINGS:SetPlayerMenuOff()

MESSAGE:New("Placing map markers. Mission Version : V2.5.1; Script Version : V1.1", 5, "INFO"):ToAll()

---------------------------------------------
-- Range Markers
---------------------------------------------

-- Range Alpha
local range_alpha_coords = COORDINATE:New(-00236662, 0, -00049367)
range_alpha_marker = MARKER:New(range_alpha_coords, "RANGE ALPHA : Strafing pits, \nInstructor Radio : 350.000 MHz AM, \nRange Radio : 351.000 MHz AM,\n Hot Range"):ReadOnly():ToAll()

-- Range Bravo
local range_bravo_coords = COORDINATE:New(-00187594, 0, 00036074)
range_bravo_marker = MARKER:New(range_bravo_coords, "RANGE BRAVO : Smart Bombs, \nInstructor Radio : 350.100 MHz AM, \nRange Radio : 351.100 MHz AM,\n Hot Range"):ReadOnly():ToAll()

-- Range Charlie
local range_charlie_coords = COORDINATE:New(-00000830, 0, 00000341)
range_charlie_marker = MARKER:New(range_charlie_coords, "RANGE CHARLIE : Missiles, \nInstructor Radio : 350.200 MHz AM, \nRange Radio : 351.200 MHz AM,\n Hot Range"):ReadOnly():ToAll()

-- Range Delta
local range_delta_coords = COORDINATE:New(-00071817, 0, -00501874)
range_delta_marker = MARKER:New(range_delta_coords, "RANGE DELTA : Dumb Bombs and rockets, \nInstructor Radio : 350.300 MHz AM, \nRange Radio : 351.300 MHz AM,\n Hot Range"):ReadOnly():ToAll()

-- Range Echo
local range_echo_coords = COORDINATE:New(00161400, 0, -00562677)
range_echo_marker = MARKER:New(range_echo_coords, "RANGE ECHO : Anti-Shipping Range, \nInstructor Radio : 350.400 MHz AM, \nRange Radio : 351.400 MHz AM,\n Hot Range"):ReadOnly():ToAll()

-- Range Foxtrot : EWR
local range_fox_ewr_coords = COORDINATE:New(-00372132, 0, -00269144)
range_fox_ewr_marker = MARKER:New(range_fox_ewr_coords, "RANGE FOXTROT : EWR Range, \nInstructor Radio : 350.500 MHz AM, \nRange Radio : 351.500 MHz AM,\n Hot Range"):ReadOnly():ToAll()

-- Range Foxtrot : Launchers + SHORAD
local range_fox_launchers_shorad_coords = COORDINATE:New(-00386577, 0, -00174975)
range_fox_launchers_shorad_marker = MARKER:New(range_fox_launchers_shorad_coords, "RANGE FOXTROT : Launchers + SHORAD, \nInstructor Radio : 350.600 MHz AM, \nRange Radio : 351.600 MHz AM,\n Hot Range"):ReadOnly():ToAll()

-- Range Foxtrot : SR
local range_fox_sr_coords = COORDINATE:New(-00399824, 0, -00102537)
range_fox_sr_marker = MARKER:New(range_fox_sr_coords, "RANGE FOXTROT : Search Radars, \nInstructor Radio : 350.700 MHz AM, \nRange Radio : 351.700 MHz AM,\n Hot Range"):ReadOnly():ToAll()

-- Range Foxtrot : Track & ST Radars
local range_fox_t_st_coords = COORDINATE:New(-00383466, 0, -00031794)
range_fox_t_st_marker = MARKER:New(range_fox_t_st_coords, "RANGE FOXTROT : Track and Search & Track Radars, \nInstructor Radio : 350.800 MHz AM, \nRange Radio : 351.800 MHz AM,\n Hot Range"):ReadOnly():ToAll()

MESSAGE:New("Placing map markers. Mission Version : V2.5.2 Script Version : V1.1", 5, "END"):ToAll()