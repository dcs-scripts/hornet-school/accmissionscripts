-- No MOOSE settings menu. Comment out this line if required.
_SETTINGS:SetPlayerMenuOff()

MESSAGE:New("Loading Airboss Script. Mission Version : V2.5 . Script Version : v1.5", 15, "INFO"):ToAll()

Roosevelt = NAVYGROUP:New("Roosevelt")
Roosevelt:Activate()

Invincible_Group = NAVYGROUP:New("Invincible")
Invincible_Group:Activate()

--[[
AbeLincoln_Group = NAVYGROUP:New("AbeLincoln")
AbeLincoln_Group:Activate()

Stennis_Group = NAVYGROUP:New("Stennis")
Stennis_Group:Activate()
]]--

global_time_recovery_end = 0

ship_dictionary = {Roosevelt = {NavyGroup = Roosevelt, ShipCallsign = "Rough Rider", TimeRecoveryEnd=0}, 
                   Invincible = {NavyGroup = Invincible_Group, ShipCallsign = "Invincible", TimeRecoveryEnd=0} }
                   -- AbeLincoln = {NavyGroup = AbeLincoln_Group, ShipCallsign = "Union"}, 
                   -- Stennis = {NavyGroup = Stennis_Group, ShipCallsign = "Courage"} }


-----------------------------------------------------------------------------------------------------------------------------------------
--TDR Beacon and Lights
-----------------------------------------------------------------------------------------------------------------------------------------

-- TDR CVN Beacons and Lights START
-- Flags TCN = 100,101,
-- Flags ICLS = 102,103,
-- Flags Link 4 = 104,105,
-- Flags ACLS = 106,107,
-- Flags Lights = 108,109,110,111,112

tdr_messages = {TCN = {OFF="TDR TACAN Deactivated", ON="TDR TACAN Restarted"},
                ICLS = {OFF="TDR ICLS Deactivated", ON="TDR ICLS Restarted"},
                LINK4 = {OFF="TDR Link 4 Deactivated", ON="TDR Link 4 Restarted"},
                ACLS = {OFF="TDR ACLS Deactivated", ON="TDR ACLS Restarted"},
                LIGHTS = {OFF="TDR Lights off", AUTO="TDR Lights set to Auto", NAVIGATION="TDR Lights set to Navigation", LAUNCH="TDR Lights set to Launch", RECOVERY="TDR Lights set to Recovery"}}

tdr_flags = {TCN = {OFF="100", ON="101"},
             ICLS = {OFF="102", ON="103"},
             LINK4 = {OFF="104", ON="105"},
             ACLS = {OFF="106", ON="107"},
             LIGHTS = {OFF="108", AUTO="109", NAVIGATION="110", LAUNCH="111", RECOVERY="112"},
             MESSAGES = tdr_messages}

-----------------------------------------------------------------------------------------------------------------------------------------
--R05 Beacon and Lights
-----------------------------------------------------------------------------------------------------------------------------------------

-- R05 CVN Beacons and Lights START
-- Flags TCN = 200,201
-- Flags Lights = 202,203,204,205,206

r05_messages = {TCN = {OFF="R05 TACAN Deactivated"}, ON={"R05 TACAN Restarted"},
                LIGHTS = {OFF="R05 Lights Off", AUTO="R05 Lights set to Auto", NAVIGATION="R05 Lights set to Navigation", LAUNCH="R05 Lights set to Launch", RECOVERY="R05 Lights set to recovery"}}

r05_flags = {TCN = {OFF="200", ON="201"},
             LIGHTS = {OFF="202", AUTO="203", NAVIGATION="204", LAUNCH="205", RECOVERY="206"},
             MESSAGES = r05_messages}

-----------------------------------------------------------------------------------------------------------------------------------------
--Airboss Menu functions
-----------------------------------------------------------------------------------------------------------------------------------------


function stopRecovery(ship_name)
  ship_dictionary[ship_name]["NavyGroup"]:TurnIntoWindStop()
  
  MESSAGE:New("99 "..ship_dictionary[ship_name]["ShipCallsign"].." recovery opperations complete, returning to base course", 15, "INFO"):ToAll()
end

function getTimeRemaining(ship_name)

   local time_now = timer.getAbsTime()
   timer_recovery_end = UTILS.ClockToSeconds(ship_dictionary[ship_name]["TimeRecoveryEnd"])

   local time_left = timer_recovery_end - time_now

   local time_left_hours = math.floor(time_left / 3600)
   local time_left_min = math.floor(time_left % 3600 / 60)
   local time_left_sec = math.floor(time_left % 3600 % 60)

   MESSAGE:New("Time till turn to base recovery course (HH:MM) "..time_left_hours..":"..time_left_min, 15, "INFO"):ToAll()
end

function startRecovery(recovery_parameters)

   local recovery_time = recovery_parameters[1]
   local ship_name = recovery_parameters[2]

   local time_now = timer.getAbsTime()
   local time_end = time_now + recovery_time*60

   local steaming_into_wind = ship_dictionary[ship_name]["NavyGroup"]:IsSteamingIntoWind()

   if (steaming_into_wind) then
      MESSAGE:New("99 "..ship_dictionary[ship_name]["ShipCallsign"].." is already steaming into the wind till "..ship_dictionary[ship_name]["TimeRecoveryEnd"], 15, "INFO"):ToAll()
   else
      local timer_recovery_start = UTILS.SecondsToClock(time_now, false)
      timer_recovery_end = UTILS.SecondsToClock(time_end, false)
      ship_dictionary[ship_name]["TimeRecoveryEnd"] = timer_recovery_end

      ship_dictionary[ship_name]["NavyGroup"]:AddTurnIntoWind(timer_recovery_start, timer_recovery_end, 25, false, -9)
      MESSAGE:New("99 "..ship_dictionary[ship_name]["ShipCallsign"].." turning, at time "..timer_recovery_start.." until "..timer_recovery_end, 15, "INFO"):ToAll()
   end
end

function checkSteamingIntoWind(ship_name)
   local steaming_into_wind = ship_dictionary[ship_name]["NavyGroup"]:IsSteamingIntoWind()

   if (steaming_into_wind) then
      MESSAGE:New("99 "..ship_dictionary[ship_name]["ShipCallsign"].." is steaming into the wind.", 15, "INFO"):ToAll()
   else
      MESSAGE:New("99 "..ship_dictionary[ship_name]["ShipCallsign"].."r is on normal course.", 15, "INFO"):ToAll()
   end
end

function beaconsLightsFlag(ship_dict_beacon_flag)

   local ship_dict     = ship_dict_beacon_flag[1]
   local ship_beacon   = ship_dict_beacon_flag[2]
   local beacon_flag   = ship_dict_beacon_flag[3]

   local beacon_on = USERFLAG:New(ship_dict[ship_beacon][beacon_flag]):Set(true)

   MESSAGE:New(ship_dict["MESSAGES"][ship_beacon][beacon_flag], 15, "INFO"):ToAll()
end

MESSAGE:New("Loading Menu Options for the Carriers and LHAs", 15, "INFO"):ToAll()

--Main Menu
TopMenu1 = MENU_COALITION:New(coalition.side.BLUE, "Carrier Menus" )

-----------------------------------------------------------------------------------------------------------------------------------------
--TDR Menu 
-----------------------------------------------------------------------------------------------------------------------------------------

--Level 1
tdr_menu = MENU_COALITION:New(coalition.side.BLUE, "TDR" , TopMenu1)

--Level 2

tdr_course = MENU_COALITION:New(coalition.side.BLUE, "Turn into the Wind" , tdr_menu)
tdr_beacon = MENU_COALITION:New(coalition.side.BLUE, "Cycle CVN Beacons" , tdr_menu)
tdr_lights = MENU_COALITION:New(coalition.side.BLUE, "Cycle CVN Lights" , tdr_menu)

--Level 3

tdr_base_course = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Stop Recovery and return to base course", tdr_course, stopRecovery, "Roosevelt")
Menu1112 = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Request CVN turn into the wind for 30 minutes", tdr_course, startRecovery, {30, "Roosevelt"})
Menu1113 = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Request CVN turn into the wind for 60 minutes", tdr_course, startRecovery, {60, "Roosevelt"})
Menu1114 = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Request CVN turn into the wind for 90 minutes", tdr_course, startRecovery, {90, "Roosevelt"})
Menu1116 = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Check for steaming into wind", tdr_menu, checkSteamingIntoWind, "Roosevelt")
Menu1117 = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Get remaining time for recovery", tdr_menu, getTimeRemaining, "Roosevelt")

tdr_tacan = MENU_COALITION:New(coalition.side.BLUE, "TACAN" , tdr_beacon) -- Menu1121
tdr_icls = MENU_COALITION:New(coalition.side.BLUE, "ICLS" , tdr_beacon) -- Menu 1122
tdr_link4 = MENU_COALITION:New(coalition.side.BLUE, "LINK 4" , tdr_beacon) -- Menu 1123
tdr_acls = MENU_COALITION:New(coalition.side.BLUE, "ACLS" , tdr_beacon) -- Menu 1124

tdr_lights_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Turn Off CVN Lights", tdr_lights, beaconsLightsFlag, {tdr_flags, "LIGHTS", "OFF"})
tdr_lights_auto = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Set CVN Lights to Auto", tdr_lights, beaconsLightsFlag, {tdr_flags, "LIGHTS", "AUTO"})
tdr_lights_navigation = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Set CVN Lights to Navigation", tdr_lights, beaconsLightsFlag, {tdr_flags, "LIGHTS", "NAVIGATION"})
tdr_lights_launch = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Set CVN Lights for Launch", tdr_lights, beaconsLightsFlag, {tdr_flags, "LIGHTS", "LAUNCH"})
tdr_lights_reconvery = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Set CVN Lights for Recovery", tdr_lights, beaconsLightsFlag, {tdr_flags, "LIGHTS", "RECOVERY"})

--Level 4
tdr_tacan_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Deactivate TACAN", tdr_tacan, beaconsLightsFlag, {tdr_flags, "TCN", "OFF"})
tdr_tacan_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Restart TACAN", tdr_tacan, beaconsLightsFlag, {tdr_flags, "TCN", "ON"})
tdr_icls_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Deactivate ICLS", tdr_icls, beaconsLightsFlag, {tdr_flags, "ICLS", "OFF"})
tdr_icls_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Restart ICLS", tdr_icls, beaconsLightsFlag, {tdr_flags, "ICLS", "ON"})
tdr_link4_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Deactivate LINK 4", tdr_link4, beaconsLightsFlag, {tdr_flags, "LINK4", "OFF"})
tdr_lin4_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Restart LINK 4", tdr_link4, beaconsLightsFlag, {tdr_flags, "LINK4","ON"})
tdr_acls_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Deactivate ACLS", tdr_acls, beaconsLightsFlag, {tdr_flags, "ACLS", "OFF"})
tdr_acls_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Restart ACLS", tdr_acls, beaconsLightsFlag, {tdr_flags, "ACLS", "ON"})

-- GWA End

-----------------------------------------------------------------------------------------------------------------------------------------
--R05 Menu
-----------------------------------------------------------------------------------------------------------------------------------------

-- Level 1
invincible_menu11 = MENU_COALITION:New(coalition.side.BLUE, "R05", TopMenu1)

-- Level 2

invincible_course = MENU_COALITION:New(coalition.side.BLUE, "Turn into the wind", invincible_menu11) -- Course Changes
invincible_beacons = MENU_COALITION:New(coalition.side.BLUE, "Cycle R05 Beacons", invincible_menu11) -- Beacons
invincible_lights = MENU_COALITION:New(coalition.side.BLUE, "Cycle R05 Lights", invincible_menu11) -- Lights

-- Level 3
-- Course Changes R05
invincible_menu1111 = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Stop Recovery and return to base course", invincible_course, stopRecovery, "Invincible")
invincible_menu1112 = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Request R05 turn into the wind for 30 minutes", invincible_course, startRecovery, {30, "Invincible"})
invincible_menu1113 = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Request R05 turn into the wind for 60 minutes", invincible_course, startRecovery, {60, "Invincible"})
invincible_menu1114 = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Request R05 turn into the wind for 90 minutes", invincible_course, startRecovery, {90, "Invincible"})
invincible_menu1115 = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Check for steaming into wind", invincible_menu11, checkSteamingIntoWind, "Invincible")
invincible_menu1115 = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Get remaining time for recovery", invincible_menu11, getTimeRemaining, "Invincible")

-- Beacons
invincible_tcn = MENU_COALITION:New(coalition.side.BLUE, "TACAN", invincible_beacons)
invincible_tcn_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Deactivate TACAN", invincible_tcn, beaconsLightsFlag, {r05_flags, "TCN", "OFF"})
invincible_tcn_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Restart TACAN", invincible_tcn, beaconsLightsFlag, {r05_flags, "TCN", "ON"})

-- Lights
invincible_lights_off_menu = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Turn off R05 Lights", invincible_lights, beaconsLightsFlag, {r05_flags, "LIGHTS", "OFF"})
invincible_lights_auto_menu = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "R05 Lights to Auto", invincible_lights, beaconsLightsFlag, {r05_flags, "LIGHTS", "AUTO"})
invincible_lights_navigation_menu = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "R05 Lights to Navigation", invincible_lights, beaconsLightsFlag, {r05_flags, "LIGHTS", "NAVIGATION"})
invincible_lights_launch_menu = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "R05 Lights to Launch", invincible_lights, beaconsLightsFlag, {r05_flags, "LIGHTS", "LAUNCH"})
invincible_lights_recovery_menu = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "R05 Lights to Recovery", invincible_lights, beaconsLightsFlag, {r05_flags, "LIGHTS", "RECOVERY"})

MESSAGE:New("Loading Airboss Script. Mission Version : V2.5 . Script Version : v1.5", 10, "END"):ToAll()