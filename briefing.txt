Welcome to The Hornets Nest : ACCT Version
(This server resets every 24 hours)

-Restricted to everyone to only ACCT trainees and mentors.
-Pilots are encouraged to use ATC frequencies to announce takeoff and landing intentions.
-Pilots are encouraged to listen to the ASOS prior to landing or taking-off.
-Pilots may use whatever armaments they wish.
-Pilots should takeoff and land in accordance with wind speed at the airport or in accordance with ATC instructions.
-Pilots should monitory commmand/central channel upon takeoff and state intentions upon entering a range.

The Hornet School Discord:     https://discord.gg/uj3QTEPRdk

When using the Airboss script to recover planes on the TDR, set it back to normal course via the command menu after completing recovery operations.

Mission Details
-------------------------------------------------------------------------------------

ATC Radar  : 245.000 MHz
ATC Tower  : 245.700 MHz
ATC Ground : 245.500 MHz

Command Channel : 285.000 MHz
Interflight Comms A : 230.000 MHz

AI AWACS, Overlord 1-1 : 133.000 MHz

Ranges: All range frequencies are in AM.

1. Range Alpha : Strafing and rocket targets from the 476th Range targets.
    a. Instructor Radio : 350.000 MHz
    b. Range Radio : 351.000 MHz

2. Range Bravo: Immortal ships, bunkers, and other static objects for smart bombs
    a. Instructor Radio : 350.100 MHz
    b. Range Radio : 351.100 MHz

3. Range Charlie : Same as above but for smart missiles, LG or IR
    a. Instructor Radio : 350.200 MHz
    b. Range Radio : 351.200 MHz

4. Range Delta : Dumb bombing range
    a. Instructor Radio : 350.300 MHz
    b. Range Radio : 351.300 MHz

5. Range Echo : Anti-shipping range
    a. Instructor Radio : 350.400
    b. Range Radio : 351.400

6. Range Foxtrot : SEAD/DEAD range
    a. Range Foxtrot : EWR
        i. Instructor Radio : 350.500 MHz
        ii. Range Radio : 351.500 MHz

    b. Range Foxtrot : Launchers+TEL+SHORAD
        i. Instructor Radio : 350.600 MHz
        ii. Range Radio : 351.600 MHz

    c. Range Foxtrot : Search Radars
        i. Instructor Radio : 350.700 MHz
        ii. Range Radio : 351.700 MHz

    d. Range Foxtrot : Track Radars
        i. Instructor Radio : 350.800 MHz
        ii. Range Radio : 351.800 MHz

7. Range Golf : BVR and BFM range. Red slots are password protected.
    a. Range Radio : 275.000 MHz

-----------------------------------------------------
Airfields:

Liwa AFB
AI ATC : 250.950 MHz
TACAN : 121X OMLW
RWY : 13/31

Al Dhafra AFB
AI ATC : 251.100 MHz
TACAN : 96X
RWY : 13R/31L 13L/31R

Al Minhad AFB
AI ATC : 250.100 MHz
TACAN : 99X MIN
RWY : 09/27

Al Bateen
AI ATC : 250.600 MHz
TACAN : 116X ALB
RWY : 13/31

Sas Al Nakheel
AI ATC : 250.450 MHz
TACAN : 117X SAN
RWY : 16/34

Abu Dhabi Intl
AI ATC : 250.550 MHz
TACAN : 118X ADR (13R) / 119X ADL (13L)
RWY : 13R/31L 13L/31R

-----------------------------------------------------
Tankers:

KC-130 (Basket) Shell 1-1
Freq: 270.000 MHz
TACAN: 111Y "SH11"

KC-135MPRS (Basket & Probe) Arco 1-1
Freq: 272.200 MHz
TACAN : 110Y "AR11"

S3B Tanker (Basket) Mauler 1-1
Freq : 280.100 MHz
TACAN : 121Y M11
Recovery Tanker for TDR

-----------------------------------------------------
Carriers:

USS Theodore Roosevelt CVN-71
AI Freq: 128.500 MHz
TACAN: 71X TDR
ICLS Channel: 11
Link4: 336

Human Marshall : 310.000 MHz
Human Tower/LSO : 307.500
Human Approach : 307 (Appr A) / 307.100 (Appr B)


HMS Invincible R05
AI Freq: 227.500 MHz
TACAN: 05X R05

Human Marshall : 325.00 MHz
Human Tower/LSO : 321.500 MHz
Human Approach : 321.000 (Appr A) / 321.100 (Appr B)
-------------------------------------------------------------------------------------

Mission Editor: Fett, Orion