-- No MOOSE settings menu. Comment out this line if required.
_SETTINGS:SetPlayerMenuOff()

MESSAGE:New("Loading messages script. Mission Version : v2.5 . Script Version : v1.0", 5, "INFO"):ToAll()

function serverReset(time_reset)
    local timenow=timer.getAbsTime( )
    local timeend = timenow + time_reset*60

    timeend = UTILS.SecondsToClock(timeend,false)
    MESSAGE:New("Server will restart in "..time_reset.." at "..timeend, 60, "INFO"):ToAll()
end

function serverRestartTimeLeft()
    local time_now = timer.getTime()
    local time_left = 86400 - time_now

    -- MESSAGE:New("Time left in seconds : "..time_left, 15, "INFO"):ToAll()
    -- MESSAGE:New("Time left datatype "..type(time_left), 15, "INFO"):ToAll()

    local time_left_hours = math.floor(time_left / 3600)
    local time_left_min = math.floor(time_left % 3600 / 60)
    local time_left_sec = math.floor(time_left % 3600 % 60)

    MESSAGE:New("Time till server restart "..time_left_hours..":"..time_left_min..":"..time_left_sec, 15,"INFO"):ToAll()
end

-- Main Menu
server_messages_menu = MENU_COALITION:New(coalition.side.BLUE, "Server Message")

-- Level 1
time_left_restart_menu = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Time till restart", server_messages_menu, serverRestartTimeLeft)