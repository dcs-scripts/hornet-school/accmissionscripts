MESSAGE:New("Loading Random Air Traffic. Version 1.6.8. Mission Version : V2.5.2", 10, "INFO"):ToAll()

-----------------------------------------------------------------------------------------------------------------------------------------
-- Civilian Traffic
-----------------------------------------------------------------------------------------------------------------------------------------

-- Planes list : RAT_An-26B, RAT_An-30M, RAT_Yak-40


local an26b = RAT:New("RAT_An-26B")
an26b:SetCoalitionAircraft("neutral")
an26b:ContinueJourney()
an26b:RadioFrequency(122.800)
an26b:RadioModulation("FM")
an26b:RadioON()
an26b:SetMinDistance(80)
an26b:Spawn(3)
an26b:ATC_Messages(false)
-- an26b:SetTakeoff("air")

local an30m = RAT:New("RAT_An-30M")
an30m:SetCoalitionAircraft("neutral")
an30m:ContinueJourney()
an30m:RadioFrequency(122.800)
an30m:RadioModulation("FM")
an30m:RadioON()
an30m:SetMinDistance(80)
an30m:Spawn(3)
an30m:ATC_Messages(false)
-- an30m:SetTakeoff("air")

local yak40 = RAT:New("RAT_Yak-40")
yak40:SetCoalitionAircraft("neutral")
yak40:ContinueJourney()
yak40:RadioFrequency(122.800)
yak40:RadioModulation("FM")
yak40:RadioON()
yak40:SetMinDistance(80)
yak40:Spawn(3)
yak40:ATC_Messages(false)
-- yak40:SetTakeoff("air")

-----------------------------------------------------------------------------------------------------------------------------------------
-- Blue Traffic
-----------------------------------------------------------------------------------------------------------------------------------------

-- Planes List : RAT_C130, RAT_Tornado, RAT_UH1H

local c130 = RAT:New("RAT_C130")
c130:SetCoalitionAircraft("blue")
c130:SetCoalition("sameonly")
c130:ContinueJourney()
c130:RadioFrequency(122.800)
c130:RadioModulation("FM")
c130:RadioON()
c130:SetMinDistance(100)
c130:SetTerminalType(AIRBASE.TerminalType.OpenBig)
c130:ExcludedAirports("Sir Abu Nuayr")
c130:Spawn(3)

local tornado = RAT:New("RAT_Tornado")
tornado:SetCoalitionAircraft("blue")
tornado:SetCoalition("sameonly")
tornado:ContinueJourney()
tornado:RadioFrequency(122.800)
tornado:RadioModulation("FM")
tornado:RadioON()
tornado:SetMinDistance(100)
tornado:Spawn(3)

local uh60a = RAT:New("RAT_UH60A")
uh60a:SetCoalitionAircraft("blue")
uh60a:SetCoalition("sameonly")
-- uh60a:SetTakeoff("hot")
uh60a:SetFLcruise(7)
uh60a:SetDeparture({"Liwa AFB", "Al Dhafra AFB", "Al Minhad AFB", "Roosevelt", "Invincible"})
uh60a:SetDestination("Liwa AFB", "Al Dhafra AFB", "Al Minhad AFB", "Roosevelt", "Invincible")
uh60a:Spawn(3)

-----------------------------------------------------------------------------------------------------------------------------------------
-- Red Traffic
-----------------------------------------------------------------------------------------------------------------------------------------

-- Planes List : RAT_Tu-22, RAT_Mig29A, RAT_F1C

local tu22 = RAT:New("RAT_Tu-22")
tu22:SetCoalitionAircraft("red")
tu22:SetCoalition("sameonly")
tu22:ContinueJourney()
tu22:RadioFrequency(122.800)
tu22:RadioModulation("FM")
tu22:RadioON()
tu22:SetMinDistance(120)
tu22:ATC_Messages(false)
tu22:Spawn(3)

local mig29a = RAT:New("RAT_Mig29A")
mig29a:SetCoalitionAircraft("red")
mig29a:SetCoalition("sameonly")
mig29a:ContinueJourney()
mig29a:RadioFrequency(122.800)
mig29a:RadioModulation("FM")
mig29a:RadioON()
mig29a:SetMinDistance(120)
mig29a:ATC_Messages(false)
mig29a:Spawn(3)

local f1c = RAT:New("RAT_F1C")
f1c:SetCoalitionAircraft("red")
f1c:SetCoalition("sameonly")
f1c:ContinueJourney()
f1c:RadioFrequency(122.800)
f1c:RadioModulation("FM")
f1c:RadioON()
f1c:SetMinDistance(120)
f1c:ATC_Messages(false)
f1c:Spawn(3)

MESSAGE:New("Let the Chaos ensue. Version 1.6.8. Mission Version : v2.5.2", 10, "INFO"):ToAll()