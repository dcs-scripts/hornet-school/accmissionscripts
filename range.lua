-----------------------------------------------------------------------------------------------------------------------------------------
-- Range Delta
-----------------------------------------------------------------------------------------------------------------------------------------

 -- Table of bombing target names. Again these are the names of the corresponding units as defined in the ME.
local range_delta_targets = {"Range-Delta-Centre", "Range-Delta-Bomb-1", "Range-Delta-Bomb-2", "Range-Delta-Bomb-3", "Range-Delta-Bomb-4", "Range-Delta-Bomb-5", "Range-Delta-Bomb-6", "Range-Delta-Bomb-7", "Range-Delta-Bomb-8", "Range-Delta-Bomb-9", "Range-Delta-Bomb-10", "Range-Delta-Bomb-11", "Range-Delta-Bomb-12", "Range-Delta-Bomb-13"}

-- local range_delta_targets = {"Range-Delta-Bomb-1", "Range-Delta-Bomb-2", "Range-Delta-Bomb-13"}

 -- Create a range object.
range_delta=RANGE:New("Range Delta")

 -- Distance between strafe target and foul line. You have to specify the names of the unit or static objects.
 -- Note that this could also be done manually by simply measuring the distance between the target and the foul line in the ME.
 -- GoldwaterRange:GetFoullineDistance("GWR Strafe Pit Left 1", "GWR Foul Line Left")

 -- Add strafe pits. Each pit (left and right) consists of two targets. Where "nil" is used as input, the default value is used.

 -- Add bombing targets. A good hit is if the bomb falls less then 50 m from the target.
range_delta:AddBombingTargets(range_delta_targets, 50)

-- Range Preferences
-- range_delta:TrackRocketsOFF()
range_delta:SetRangeRadius(27)
range_delta:SetMaxStrafeAlt(10000)
-- range_delta:SetRangeZone(ZONE:New("Range-Delta-Zone"))
range_delta:SetMessageTimeDuration(10)
range_delta:SetInstructorRadio(350.300)
range_delta:SetRangeControl(351.300)

 -- Start range.
range_delta:Start()

MESSAGE:New("Range Delta Initialised", 10, "INFO"):ToAll()



-----------------------------------------------------------------------------------------------------------------------------------------
-- Range Alpha
-----------------------------------------------------------------------------------------------------------------------------------------

range_alpha = RANGE:New("Range Alpha")
zone_range_alpha = ZONE:New("Zone-Range-Alpha")
range_alpha:SetRangeZone(zone_range_alpha)

local range_alpha_sp_1 = {"Range-Alpha-SP-1", "Range-Alpha-FL-1"}
local range_alpha_sp_1_foulline = range_alpha:GetFoullineDistance("Range-Alpha-SP-1", "Range-Alpha-FL-1")
range_alpha:AddStrafePit(range_alpha_sp_1, 7000, 500, 270, false, 20, range_alpha_sp_1_fouline)

local range_alpha_sp_2 = {"Range-Alpha-SP-2", "Range-Alpha-FL-2"}
local range_alpha_sp_2_foulline = range_alpha:GetFoullineDistance("Range-Alpha-SP-2", "Range-Alpha-FL-2")
range_alpha:AddStrafePit(range_alpha_sp_2, 7000, 500, 180, false, 20, range_alpha_sp_2_foulline)

local range_alpha_sp_3 = {"Range-Alpha-SP-3", "Range-Alpha-FL-3"}
local range_alpha_sp_3_foulline = range_alpha:GetFoullineDistance("Range-Alpha-SP-3", "Range-Alpha-FL-3")
range_alpha:AddStrafePit(range_alpha_sp_3, 7000, 500, 90, false, 20, range_alpha_sp_3_foulline)

local range_alpha_sp_4 = {"Range-Alpha-SP-4", "Range-Alpha-FL-4"}
local range_alpha_sp_4_foulline = range_alpha:GetFoullineDistance("Range-Alpha-SP-4", "Range-Alpha-FL-4")
range_alpha:AddStrafePit(range_alpha_sp_4, 7000, 500, 51, false, 20, range_alpha_sp_4_foulline)

local range_alpha_sp_5 = {"Range-Alpha-SP-5", "Range-Alpha-FL-5"}
local range_alpha_sp_5_foulline = range_alpha:GetFoullineDistance("Range-Alpha-SP-5", "Range-Alpha-FL-5")
range_alpha:AddStrafePit(range_alpha_sp_5, 7000, 500, 124, false, 20, range_alpha_sp_5_foulline)

local range_alpha_sp_6 = {"Range-Alpha-SP-6", "Range-Alpha-FL-6"}
local range_alpha_sp_6_foulline = range_alpha:GetFoullineDistance("Range-Alpha-SP-6", "Range-Alpha-FL-6")
range_alpha:AddStrafePit(range_alpha_sp_6, 7000, 500, 224, false, 20, range_alpha_sp_6_foulline)

local range_alpha_sp_7 = {"Range-Alpha-SP-7", "Range-Alpha-FL-7"}
local range_alpha_sp_7_foulline = range_alpha:GetFoullineDistance("Range-Alpha-SP-7", "Range-Alpha-FL-7")
range_alpha:AddStrafePit(range_alpha_sp_7, 7000, 500, 312, false, 20, range_alpha_sp_7_foulline)

local range_alpha_sp_8 = {"Range-Alpha-SP-8", "Range-Alpha-FL-8"}
local range_alpha_sp_8_foulline = range_alpha:GetFoullineDistance("Range-Alpha-SP-8", "Range-Alpha-FL-8")
range_alpha:AddStrafePit(range_alpha_sp_8, 7000, 500, 345, false, 20, range_alpha_sp_8_foulline)

local range_alpha_sp_9 = {"Range-Alpha-SP-9", "Range-Alpha-FL-9"}
local range_alpha_sp_9_foulline = range_alpha:GetFoullineDistance("Range-Alpha-SP-9", "Range-Alpha-FL-9")
range_alpha:AddStrafePit(range_alpha_sp_9, 7000, 500, 247, false, 20, range_alpha_sp_9_foulline)

range_alpha:TrackRocketsOFF()
range_alpha:SetMaxStrafeAlt(1500)
range_alpha:SetMessageTimeDuration(10)

range_alpha:SetInstructorRadio(350)
range_alpha:SetRangeControl(351)

range_alpha:Start()

MESSAGE:New("Range Alpha Initialised", 10, "INFO"):ToAll()


-----------------------------------------------------------------------------------------------------------------------------------------
-- Range Bravo - Armour
-----------------------------------------------------------------------------------------------------------------------------------------

-- Targets : Range-Bravo-3, Range-Bravo-1, Range-Bravo-2-1-8, Range-Bravo-4, Range-Bravo-5, Range-Bravo-6, Range-Bravo-7, Range-Bravo-8-1-3, Range-Bravo-9, Range-Bravo-10

local range_bravo_targets = {"Range-Bravo-3", "Range-Bravo-1", "Range-Bravo-2-1", "Range-Bravo-2-2", "Range-Bravo-2-3", "Range-Bravo-2-4", "Range-Bravo-2-5", "Range-Bravo-2-6", "Range-Bravo-2-7", "Range-Bravo-2-8", "Range-Bravo-4", "Range-Bravo-5", "Range-Bravo-6", "Range-Bravo-7", "Range-Bravo-8-1", "Range-Bravo-8-2", "Range-Bravo-8-3", "Range-Bravo-9", "Range-Bravo-10"}

range_bravo = RANGE:New("Range Bravo")

range_bravo:AddBombingTargets(range_bravo_targets, 15)

range_bravo:TrackRocketsOFF()
range_bravo:SetRangeRadius(46)
range_bravo:SetBombtrackThreshold(46)
range_bravo:SetMessageTimeDuration(10)

range_bravo:SetInstructorRadio(350.100)
range_bravo:SetRangeControl(351.100)

range_bravo:Start()

MESSAGE:New("Range Bravo Initialised", 10, "INFO"):ToAll()


-----------------------------------------------------------------------------------------------------------------------------------------
-- Range Charlie - Static Airplanes and Helicopters
-----------------------------------------------------------------------------------------------------------------------------------------

-- Targets : Range-Charlie-2, Range-Charlie-1-4, Range-Charlie-3, Range-Charlie-4, Range-Charlie-5, Range-Charlie-6, Range-Charlie-7

local range_charlie_targets = {"Range-Charlie-2", "Range-Charlie-1-1", "Range-Charlie-1-2", "Range-Charlie-1-3", "Range-Charlie-1-4", "Range-Charlie-3", "Range-Charlie-4", "Range-Charlie-5", "Range-Charlie-6", "Range-Charlie-7"}

range_charlie = RANGE:New("Range Charlie")

range_charlie:AddBombingTargets(range_charlie_targets, 20)

range_charlie:TrackRocketsOFF()
range_charlie:SetRangeRadius(46)
range_charlie:SetBombtrackThreshold(46)
range_charlie:SetMessageTimeDuration(10)

range_charlie:SetInstructorRadio(350.200)
range_charlie:SetRangeControl(351.200)

range_charlie:Start()

MESSAGE:New("Range Charlie Initialised", 10, "INFO"):ToAll()

-----------------------------------------------------------------------------------------------------------------------------------------
-- Range Echo - Ships
-----------------------------------------------------------------------------------------------------------------------------------------

-- Targets : Range-Echo-Centre, Range-Echo-1-7

local range_echo_targets = {"Range-Echo-Centre", "Range-Echo-1", "Range-Echo-2", "Range-Echo-3", "Range-Echo-4", "Range-Echo-5", "Range-Echo-6", "Range-Echo-7"}

range_echo = RANGE:New("Range Echo")

range_echo:AddBombingTargets(range_echo_targets, 100)

range_echo:TrackRocketsOFF()
range_echo:SetRangeRadius(10)
range_echo:SetBombtrackThreshold(120)
range_echo:SetMessageTimeDuration(10)

range_echo:SetInstructorRadio(350.400)
range_echo:SetRangeControl(351.400)

range_echo:Start()

MESSAGE:New("Range Echo Initialised", 10, "INFO"):ToAll()

-----------------------------------------------------------------------------------------------------------------------------------------
-- Range Foxtrot
-----------------------------------------------------------------------------------------------------------------------------------------

-----------------
-- Range Foxtrot : EWR
-----------------

local range_foxtrot_ewr_targets = {"Range-Foxtrot-55G6", "Range-Foxtrot-1L13"}

range_foxtrot_ewr = RANGE:New("Range Foxtrot : EWR")

range_foxtrot_ewr:AddBombingTargets(range_foxtrot_ewr_targets, 30)

range_foxtrot_ewr:TrackRocketsOFF()
range_foxtrot_ewr:SetRangeRadius(30)
range_foxtrot_ewr:SetBombtrackThreshold(150)
range_foxtrot_ewr:SetMessageTimeDuration(10)

range_foxtrot_ewr:SetInstructorRadio(350.500)
range_foxtrot_ewr:SetRangeControl(351.500)

range_foxtrot_ewr:Start()

-----------------
-- Range Foxtrot : Launchers + TEL + SHORAD
-----------------

local range_foxtrot_launcher_tel_shorad_targets = {"Range-Foxtrot-SA-11-TEL", "Range-Foxtrot-SA-2-LN", "Range-Foxtrot-SA-3-LN", "Range-Foxtrot-SA-5-LN", "Range-Foxtrot-SA-10-TEL-C", "Range-Foxtrot-SA-10-TEL-D", "Range-Foxtrot-SA-6-TEL", "Range-Foxtrot-SA-15", "Range-Foxtrot-SA-19", "Range-Foxtrot-SA-13-TEL", "Range-Foxtrot-SA-8-TEL", "Range-Foxtrot-SA-9-TEL"}

range_foxtrot_launcher_tel_shorad = RANGE:New("Range Foxtrot : Launchers+TEL+SHORAD")

range_foxtrot_launcher_tel_shorad:AddBombingTargets(range_foxtrot_launcher_tel_shorad_targets, 30)

range_foxtrot_launcher_tel_shorad:TrackRocketsOFF()
range_foxtrot_launcher_tel_shorad:SetRangeRadius(30)
range_foxtrot_launcher_tel_shorad:SetBombtrackThreshold(150)
range_foxtrot_launcher_tel_shorad:SetMessageTimeDuration(10)

range_foxtrot_launcher_tel_shorad:SetInstructorRadio(350.600)
range_foxtrot_launcher_tel_shorad:SetRangeControl(351.600)

range_foxtrot_launcher_tel_shorad:Start()

-----------------
-- Range Foxtrot : Search Radar
-----------------

local range_foxtrot_search_radar_targets = {"Range-Foxtrot-Dog-Ear", "Range-Foxtrot-SA-10-BB-SR", "Range-Foxtrot-SA-10-CS-SR", "Range-Foxtrot-SA-10-TR-SR", "Range-Foxtrot-SA-11-SD-SR", "Range-Foxtrot-SA-2-3-5-FF-SR", "Range-Foxtrot-SA-5-TS-SR"}

range_foxtrot_search_radar = RANGE:New("Range Foxtrot : Search Radar")

range_foxtrot_search_radar:AddBombingTargets(range_foxtrot_search_radar_targets, 30)

range_foxtrot_search_radar:TrackRocketsOFF()
range_foxtrot_search_radar:SetRangeRadius(30)
range_foxtrot_search_radar:SetBombtrackThreshold(150)
range_foxtrot_search_radar:SetMessageTimeDuration(10)

range_foxtrot_search_radar:SetInstructorRadio(350.700)
range_foxtrot_search_radar:SetRangeControl(351.700)

range_foxtrot_search_radar:Start()

-----------------
-- Range Foxtrot : Track and Search & Track Radar
-----------------

local range_foxtrot_track_search_track_radar_targets = {"Range-Foxtrot-SA-2-RF-TR", "Range-Foxtrot-AAA-Son-9", "Range-Foxtrot-SA-10-Lid-A-TR", "Range-Foxtrot-SA-10-Lid-B-TR", "Range-Foxtrot-SA-2-FS-TR", "Range-Foxtrot-SA-3-LB-TR"; "Range-Foxtrot-SA-5-TR", "Range-Foxtrot-SA-6-TR"}

range_foxtrot_track_search_track_radar = RANGE:New("Range Foxtrot : Track Radars")

range_foxtrot_track_search_track_radar:AddBombingTargets(range_foxtrot_track_search_track_radar_targets)

range_foxtrot_track_search_track_radar:TrackRocketsOFF()
range_foxtrot_track_search_track_radar:SetRangeRadius(30)
range_foxtrot_track_search_track_radar:SetBombtrackThreshold(150)
range_foxtrot_track_search_track_radar:SetMessageTimeDuration(10)

range_foxtrot_track_search_track_radar:SetInstructorRadio(350.800)
range_foxtrot_track_search_track_radar:SetRangeControl(351.800)

range_foxtrot_track_search_track_radar:Start()

MESSAGE:New("Range Foxtrot Initialised", 10, "INFO"):ToAll()


MESSAGE:New("All ranges operational. Version 3.2.2. Mission Version 2.5", 10, "INFO"):ToAll()