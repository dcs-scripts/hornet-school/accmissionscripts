-- No MOOSE settings menu. Comment out this line if required.
_SETTINGS:SetPlayerMenuOff()

MESSAGE:New("Loading ATIS messages. Mission Version : V2.4 . Script Version : V0.3", 10, "INFO"):ToAll()

---------------
-- Liwa AFB
---------------

atis_liwa_afb = ATIS:New("Liwa AFB", 261.100, 0)
-- atis_liwa_afb:SetTACAN(121)
atis_liwa_afb:SetRadioPower(200)
atis_liwa_afb:SetReportmBar(true)
atis_liwa_afb:Start()

---------------
-- Al Dhafra AFB
---------------

atis_dhafra_afb = ATIS:New("Al Dhafra AFB", 261.200, 0)
-- atis_dhafra_afb:SetTACAN(96)
atis_dhafra_afb:SetRadioPower(200)
atis_dhafra_afb:SetReportmBar(true)
atis_dhafra_afb:Start()

---------------
-- Al Minhad AFB
---------------

atis_minhad_afb = ATIS:New("Al Minhad AFB", 261.300, 0)
atis_minhad_afb:SetRadioPower(200)
atis_minhad_afb:SetReportmBar(true)
atis_minhad_afb:Start()

---------------
-- Abu Dhabi Intl
---------------

atis_abu_dhabi_intl = ATIS:New("Abu Dhabi Intl", 261.400, 0)
atis_abu_dhabi_intl:SetRadioPower(200)
atis_abu_dhabi_intl:SetTACAN(118)
atis_abu_dhabi_intl:SetReportmBar(true)
atis_abu_dhabi_intl:Start()

---------------
-- Al Bateen
---------------

atis_al_bateen = ATIS:New("Al-Bateen", 261.500, 0)
atis_al_bateen:SetRadioPower(200)
atis_al_bateen:SetTACAN(116)
atis_al_bateen:SetReportmBar(true)
atis_al_bateen:Start()

---------------
-- Sas Al Nakheel
---------------

atis_sas_al_nakheel = ATIS:New("Sas Al Nakheel", 261.550, 0)
atis_sas_al_nakheel:SetRadioPower(200)
atis_sas_al_nakheel:SetTACAN(117)
atis_sas_al_nakheel:SetReportmBar(true)
atis_sas_al_nakheel:Start()